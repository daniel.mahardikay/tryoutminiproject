const { Type, Electronic } = require('../models');
const electronic = require('../models/electronic');

class TypeController {
    static async getType(req, res) {
        try {
            const result = await Type.findAll({
                order: [
                    ['id', 'ASC']
                ],
            })

            //res.status(200).json(result);
            res.render('types.ejs', { types: result });
        } catch (err) {
            //res.status(500).json(err);
            next(err)
        }
    }

    static async addFormType(req, res) {
        try {
            res.render('addType.ejs');
        } catch {
            next(err);
        }
    }

    static async addType(req, res) {
        const { name, info, power } = req.body;
        try {
            const found = await Type.findOne({
                where: {
                    name
                }
            })
            if (found) {
                res.status(409).json({
                    msg: "Name already exist! Input another name!."
                })
            } else {
                const type = await Type.create({
                    name,
                    info,
                    power
                })
                res.redirect('/types')
            }

        } catch (err) {
            res.status(500).json(err)
        }
    }

    static async deleteType(req, res) {
        const id = req.params.id;
        try {
            const electronic = await Electronic.findOne({
                where: { TypeId: id }
            });
            if (electronic) {
                res.status(409).json({
                    msg: "Type still used in Electronics!."
                });
            } else {
                const type = await Type.destroy({
                    where: { id }
                })
                res.redirect('/types')
            }
        } catch (err) {
            res.status(500).json(err)
                // next(err)
        }
    }

    static async editFormType(req, res) {
        const id = req.params.id;
        try {
            const result = await Type.findOne({
                    where: {
                        id
                    }
                })
                // if (check) {
                //     //res.send("editForm")
                //     //res.render('editType.ejs', { Type: result });
                // } else {
                //     res.status(409).json({
                //         msg: "Id not found!."
                //     })
                // }
            res.render('editType.ejs', { type: result });
        } catch {
            res.send(err);
        }
    }

    static async editType(req, res) {
        const id = req.params.id;
        const { name, info, power } = req.body;
        try {
            const result = await Type.update({
                name,
                info,
                power
            }, {
                where: { id }
            })
            res.redirect('/types');
        } catch {
            res.status(500).json(err);
            //res.send(err)
            // next(err)
        }
    }

}

module.exports = TypeController;