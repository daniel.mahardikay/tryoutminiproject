const { Brand, Electronic } = require('../models')

class BrandController {
    static async getBrand(req, res) {
        try {
            const result = await Brand.findAll({
                    order: [
                        ['id', 'ASC']
                    ]
                })
                // res.status(200).json(result);
            res.render('brands.ejs', { brands: result });
        } catch (err) {
            res.status(500).json(err);
        }
    }

    static addFormBrand(req, res) {
        res.render('addBrand.ejs');
    }

    static async addBrand(req, res) {
        const { name, logo, country } = req.body;
        try {
            const found = await Brand.findOne({
                where: {
                    name
                }
            })
            if (found) {
                res.send("Name already exist! Try another name arigato.")
            } else {
                const brand = await Brand.create({
                    name,
                    logo,
                    country
                })
                res.redirect('/brands')
            }
        } catch (err) {
            res.status(500).json(err)
        }
    }

    static async deleteBrand(req, res) {
        const id = req.params.id;
        try {
            const electronic = await Electronic.findOne({
                where: { BrandId: id }
            });
            if (electronic) {
                res.status(409).json({
                    msg: "Brand still used in Electronics!"
                });
            } else {
                const result = await Brand.destroy({
                    where: { id }
                })
                res.redirect('/brands')
            }
        } catch (err) {
            res.send(err)
        }
    }

    static async editFormBrand(req, res) {
        const id = req.params.id;
        try {
            const result = await Brand.findOne({
                where: { id }
            })
            res.render('editBrand.ejs', { brand: result });
        } catch (err) {
            res.send(err);
        }
    }

    static async editBrand(req, res) {
        const id = req.params.id;
        const { name, logo, country } = req.body;
        try {
            const result = await Brand.update({
                name,
                logo,
                country
            }, {
                where: { id }
            })
            res.redirect('/brand');
        } catch (err) {
            res.send(err)
        }
    }
}

module.exports = BrandController;