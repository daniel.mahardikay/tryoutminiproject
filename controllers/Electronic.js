const { Electronic, Brand, Type } = require('../models');
const electronic = require('../models/electronic');

class ElectronicController {
    static async getElectronic(req, res) {
        try {
            const electronics = await Electronic.findAll({
                order: [
                    ['id', 'ASC']
                ],
                include: [
                    Brand, Type
                ]
            });
            res.render('index.ejs', { electronics });
        } catch (err) {
            res.send(err);
        }
    }

    static async addFormElectronic(req, res) {
        try {
            const brand = await Brand.findAll({
                order: [
                    ['name', 'ASC']
                ]
            });
            const type = await Type.findAll({
                order: [
                    ['name', 'ASC']
                ]
            });
            res.render('addElectronic.ejs', { brands: brand, types: type });
        } catch (err) {
            res.status(500).json(err);
        }
    }

    static async addElectronic(req, res) {
        const { name, image, BrandId, TypeId } = req.body;
        try {
            const found = await Electronic.findOne({
                where: { name }
            });
            if (found) {
                res.status(409).json({
                    msg: "Name already exist! Try another PlayerId arigato."
                })
            } else {
                const result = await Electronic.create({
                    name,
                    image,
                    BrandId,
                    TypeId
                });
                // res.status(201).json(Electronic)
                res.redirect('/');
            }
        } catch (err) {
            res.status(500).json(err)
        }
    }

    static async deleteElectronic(req, res) {
        const id = req.params.id;
        try {
            const result = await Electronic.destroy({
                where: { id }
            })
            res.redirect('/');
        } catch (err) {
            res.status(500).json(err);
        }
    }

    static async editFormElectronic(req, res) {
        const id = req.params.id;
        try {
            const electronic = await Electronic.findOne({
                where: { id }
            });
            const brands = await Brand.findAll({
                order: [
                    ['name', 'ASC']
                ]
            });
            const types = await Type.findAll({
                order: [
                    ['name', 'ASC']
                ]
            });
            res.render('editElectronic.ejs', { electronic, brands, types });
        } catch (err) {
            res.status(500).json(err);
        }
    }

    static async editElectronic(req, res) {
        const id = req.params.id;
        const { name, image, BrandId, TypeId } = req.body;
        try {
            const electronic = await Electronic.update({
                name,
                image,
                BrandId,
                TypeId
            }, {
                where: { id }
            });
            res.redirect('/')
        } catch (err) {
            res.send(err)
        }
    }
}


module.exports = ElectronicController;