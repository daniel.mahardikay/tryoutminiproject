'use strict';
const {
    Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
    class Electronic extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            Electronic.belongsTo(models.Brand);
            Electronic.belongsTo(models.Type);
        }
    };
    Electronic.init({
        name: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: {
                    msg: "Name must be filled!"
                }
            }
        },
        image: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: {
                    msg: "Image must be filled!"
                },
                isUrl: {
                    msg: "Image must be in URL format!"
                }
            }
        },
        BrandId: DataTypes.INTEGER,
        TypeId: DataTypes.INTEGER
    }, {
        sequelize,
        modelName: 'Electronic',
    });
    return Electronic;
};