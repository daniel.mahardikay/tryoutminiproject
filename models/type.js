'use strict';
const {
    Model
} = require('sequelize');

module.exports = (sequelize, DataTypes) => {
    class Type extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
            Type.belongsToMany(models.Brand, {
                through: 'models.Electronic'
            });
        }
    };
    Type.init({
        name: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: {
                    msg: "Name cannot be empty!."
                }
            }
        },
        info: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: {
                    msg: "Information cannot be empty!."
                }
            }
        },
        power: {
            type: DataTypes.INTEGER,
            validate: {
                notEmpty: {
                    msg: "Power cannot be empty!."
                },
                isNumeric: {
                    msg: "Power must be a number."
                }
            }
        },
    }, {
        sequelize,
        modelName: 'Type',
    });
    return Type;
};