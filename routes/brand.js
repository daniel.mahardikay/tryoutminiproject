const { Router } = require('express');
const router = Router();
const BrandController = require('../controllers/Brand')

router.get('/', BrandController.getBrand);
router.get('/add', BrandController.addFormBrand);
router.post('/add', BrandController.addBrand);
router.get('/delete/:id', BrandController.deleteBrand);
router.get('/edit/:id', BrandController.editFormBrand);
router.post('/edit/:id', BrandController.editBrand);


module.exports = router;