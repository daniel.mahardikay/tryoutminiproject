const { Router } = require('express');
const router = Router();
const TypeController = require('../controllers/Type')

router.get('/', TypeController.getType);
router.get('/add', TypeController.addFormType)
router.post('/add', TypeController.addType)
router.get('/delete/:id', TypeController.deleteType)
router.get('/edit/:id', TypeController.editFormType)
router.post('/edit/:id', TypeController.editType)

module.exports = router;