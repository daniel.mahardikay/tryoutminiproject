const { Router } = require('express');
const router = Router();

const ElectronicController = require('../controllers/Electronic')
const BrandRoutes = require('./brand');
const TypeRoutes = require('./type');

router.use('/brands', BrandRoutes);
router.use('/types', TypeRoutes);

router.get('/', ElectronicController.getElectronic);
router.get('/add', ElectronicController.addFormElectronic);
router.post('/add', ElectronicController.addElectronic);
router.get('/delete/:id', ElectronicController.deleteElectronic);
router.get('/edit/:id', ElectronicController.editFormElectronic);
router.post('/edit/:id', ElectronicController.editElectronic);

module.exports = router;